import 'bootstrap/dist/css/bootstrap.min.css';

import React from 'react';
import ReactDOM from 'react-dom';

export { default as LoginForm } from "./login-form/LoginForm";
export { default as RegisterForm } from "./register-form/RegisterForm";
export { default as Menu } from "./menu/Menu";
